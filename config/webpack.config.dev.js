const CircularDependencyPlugin = require('circular-dependency-plugin');
const [ configClientCommon, configServerCommon ] = require('./webpack.config.common');
const { merge } = require('webpack-merge');

const mode = 'development';

const configClient = {
	mode,
	devtool: 'cheap-module-source-map',
	plugins: [
		new CircularDependencyPlugin({
			exclude: /node_modules/,
			failOnError: false,
			allowAsyncCycles: false,
			cwd: process.cwd(),
		})
	]
};

const configServer = {
	mode,
	devtool: 'cheap-module-source-map',
	node: {
		console: false,
		global: false,
		process: false,
		Buffer: false,
		__filename: false,
		__dirname: false,
	},
};

module.exports = [ merge(configClientCommon, configClient), merge(configServerCommon, configServer) ];