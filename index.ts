import {app} from './source/server';
import {RouterConfig} from "server/routes/config/router.config";
import {routes} from "server/routes";
import dotenv from 'dotenv';
import http from 'http';
import {routerLogger, serverLogger} from "./source/server/helper/logger.helper";
import {ServiceManager} from "./source/server/services/manager.service";
import {APP_SERVICES} from "./source/server/constant/service/service.constant";

// test 1
// test 2
// test 3

const dotenvResult = dotenv.config();

if (dotenvResult.error) {
	throw dotenvResult.error;
}

ServiceManager
	.registerServices(APP_SERVICES)
	.startServices();

const normalizePort = (val: any) => {
	const port = parseInt(val, 10);

	if (isNaN(port)) {
		// named pipe
		return val;
	}

	if (port >= 0) {
		// port number
		return port;
	}

	return false;
};

const onError = (error: any) => {
	if (error.syscall !== 'listen') {
		throw error;
	}

	const bind = typeof port === 'string'
		? 'Pipe ' + port
		: 'Port ' + port;

	// handle specific listen errors with friendly messages
	switch (error.code) {
		case 'EACCES':
			console.error(bind + ' requires privileges');
			process.exit(1);
			break;
		case 'EADDRINUSE':
			console.error(bind + ' is already in use');
			process.exit(1);
			break;
		default:
			throw error;
	}
};

const onListening = () => {
	const addr = server.address();
	const bind = typeof addr === 'string'
		? 'pipe ' + addr
		: 'port ' + addr?.port;
	serverLogger(`Server running at ${bind}`);
	routes.forEach((route: RouterConfig) => routerLogger(`Routes configured for ${route.getName()}`));
};

const port = normalizePort(process.env.SERVER_PORT || 80);
app.set('port', port);

const server = http.createServer(app);


server.on('error', onError);
server.on('listening', onListening);

export default server.listen(port);