// import * as shortUUID from 'shortid';
// import { Schema } from "mongoose";
// import { EPermissionLevel } from "../../common/enum/permission.enum";
import debug from 'debug';
import { appLogger } from "../helper/logger.helper";
import MongooseService from "../services/mongoose.service";
import { UserDto } from "../dto/user.model";
import { ServiceManager } from "../services/manager.service";
import { EServices } from "../enum/services.enum";
import { ECollections } from "../enum/collection.enum";
import { ObjectID } from "mongodb";
import omit from "lodash/omit";

const log: debug.IDebugger = appLogger.extend('User-dal');

class UsersDal {
    private static instance: UsersDal;

    private Schema = MongooseService.getMongoose().Schema;

    private userSchema = new this.Schema({
        _id: String,
        email: String,
        password: { type: String, select: false },
        firstName: String,
        lastName: String,
        permissionLevel: Number
    });

    private User = MongooseService
        .getMongoose()
        .model('Users', this.userSchema);

    constructor() {
        log('Created new instance of UsersDal');
    }

    public static getInstance() {
        if (!UsersDal.instance) {
            UsersDal.instance = new UsersDal();
        }
        return UsersDal.instance;
    }

    async addUser(userFields: Omit<UserDto, 'id'>) {
        // userFields._id = shortUUID.generate();
        // userFields.permissionLevel = EPermissionLevel.ADMIN;
        // const user = new this.User(userFields);
        // await user.save();
        // return userFields._id;

        const db = await ServiceManager
            .getService(EServices.MongoDBService)
            .getDB();
        if (db) {
            const result = await db
                .collection(ECollections.users)
                .insertOne(userFields);
            const user = result?.ops?.[0];
            return omit({ ...user, id: user._id }, '_id', 'password');
        }

        return null;
    }

    async getUserByEmail(email: string) {
        // return this.User.findOne({email: email}).exec();
        const db = await ServiceManager
            .getService(EServices.MongoDBService)
            .getDB();
        if (db) {
            return db.collection(ECollections.users).findOne({email}, {projection: {password: 0}});
        }

        return null;
    }

    async getUserByEmailWithPassword(email: string){
        // return this.User.findOne({email: email}).select('_id email permissionLevel +password').exec();
        const db = await ServiceManager
            .getService(EServices.MongoDBService)
            .getDB();
        if (db) {
            return db.collection(ECollections.users).findOne({email});
        }

        return null;
    }

    async removeUserById(userId: string) {
        return this.User.deleteOne({_id: userId}).exec();
    }

    async getUserById(userId: string) {
        // return this.User.findOne({_id: userId}).populate('User' ).exec();

        const db = await ServiceManager
            .getService(EServices.MongoDBService)
            .getDB();
        const user = await db
            .collection(ECollections.users)
            .findOne({_id: new ObjectID(userId)}, { projection: { password: 0 } });
        return omit({ ...user, id: user._id }, '_id');
    }

    async getUsers(limit: number = 25, page: number = 0) {
        return this.User.find()
            .limit(limit)
            .skip(limit * page)
            .exec();
    }

    async patchUserById(userId: string, userFields: any) {

        // const existingUser = await this.User
        //     .findOneAndUpdate({_id: userId}, {$set: userFields}, {new: true})
        //     .exec();
        //
        // if (!existingUser) {
        //     throw new Error(`User with {id: ${userId}} not found`);
        // }
        // return existingUser;
        const db = await ServiceManager
            .getService(EServices.MongoDBService)
            .getDB();
        if (db) {
            const existingUser = await db
                .collection(ECollections.users)
                .findOneAndUpdate({_id: new ObjectID(userId)}, {$set: userFields}, {projection: {password: 0}});
            if (!existingUser?.value) {
                throw new Error(`User with {id: ${userId}} not found`);
            }
            return { ...existingUser.value, ...userFields };
        }

        return null;
    }
}

export default UsersDal.getInstance();