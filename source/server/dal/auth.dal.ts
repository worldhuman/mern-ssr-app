import debug from 'debug';
import omit from "lodash/omit";
import { ECollections } from "../enum/collection.enum";
import { appLogger, errorLogger } from "../helper/logger.helper";
import { ObjectID } from "mongodb";
import { ServiceManager } from "../services/manager.service";
import { EServices } from "../enum/services.enum";

const log: debug.IDebugger = appLogger.extend('Auth-dal');

class AuthDal {
    private static instance: AuthDal;

    constructor() {
        log('Created new instance of MediaDal');
    }

    public static getInstance() {
        if (!AuthDal.instance) {
            AuthDal.instance = new AuthDal();
        }
        return AuthDal.instance;
    }

    public async addCredentials({ accessToken, userId, refreshToken }: { userId: string, accessToken: string, refreshToken: string }) {
        try {
            const db = await ServiceManager
                .getService(EServices.MongoDBService)
                .getDB();
            const result = await db
                .collection(ECollections.auth)
                .insertOne({ userId: new ObjectID(userId), accessToken, refreshToken });

            return omit(result?.ops?.[0], '_id');
        } catch (err) {
            errorLogger(err);
            throw err;
        }
    }

    public async dropCredentials({ accessToken, userId }: { userId: string, accessToken: string }) {
        try {
            const db = await ServiceManager
                .getService(EServices.MongoDBService)
                .getDB();
            const response = await db
                .collection(ECollections.auth)
                .deleteOne({ userId: new ObjectID(userId), accessToken });
            return response.deletedCount === 1;
        } catch (err) {
            errorLogger(err);
            throw err;
        }
    }

    public async getUserByIdAndAccessToken({ accessToken, userId }: { userId: string, accessToken: string }) {
        try {
            const db = await ServiceManager
                .getService(EServices.MongoDBService)
                .getDB();
            return db
                .collection(ECollections.auth)
                .findOne({ userId: new ObjectID(userId), accessToken });
        } catch (e) {
            errorLogger(e);
            throw e;
        }
    }

}

export default AuthDal.getInstance();