import debug from 'debug';
import {ECollections} from "../enum/collection.enum";
import {createClientMediaPath} from "../helper/mediaPath.helper";
import {appLogger, errorLogger} from "../helper/logger.helper";
import fs from "fs";
import {ObjectID} from "mongodb";
import {ServiceManager} from "../services/manager.service";
import {EServices} from "../enum/services.enum";

const log: debug.IDebugger = appLogger.extend('Media-dal');

class MediaDal {
    private static instance: MediaDal;

    constructor() {
        log('Created new instance of MediaDal');
    }

    public static getInstance() {
        if (!MediaDal.instance) {
            MediaDal.instance = new MediaDal();
        }
        return MediaDal.instance;
    }

    public async addMedia(file: any) {
        try {
            const db = await ServiceManager
                .getService(EServices.MongoDBService)
                .getDB();
            const result = await db.collection(ECollections.media).insertOne({
                size: file.size,
                uri: createClientMediaPath(file.filename),
                mimetype: file.mimetype,
            });

            return result?.ops?.[0]
        } catch (err) {
            errorLogger(err);
            fs.unlink(`${global.PUBLIC_PATH}/uploads/${file.filename}`, (deleteFileError) => {
                if (deleteFileError) {
                    errorLogger(deleteFileError)
                    return
                }
            });
            throw err;
        }
    }

    public async getById(id: string) {
        try {
            const db = await ServiceManager
                .getService(EServices.MongoDBService)
                .getDB();
            return db
                .collection(ECollections.media)
                .findOne(new ObjectID(id));
        } catch (e) {
            errorLogger(e);
            throw e;
        }
    }

    public async deleteById(id: string) {
        try {
            const db = await ServiceManager
                .getService(EServices.MongoDBService)
                .getDB();
            const result = await db
                .collection(ECollections.media)
                .deleteOne({_id: new ObjectID(id)})
            return result?.deletedCount === 1;
        } catch (e) {
            errorLogger(e);
            throw e;
        }
    }

    public async getList(limit: number = 20, page: number = 0) {
        try {
            const db = await ServiceManager
                .getService(EServices.MongoDBService)
                .getDB();
            return db
                .collection(ECollections.media)
                .find()
                .limit(limit)
                .skip(limit * page)
                .toArray();
        } catch (e) {
            errorLogger(e);
            throw e;
        }
    }
}

export default MediaDal.getInstance();