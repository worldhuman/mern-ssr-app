import express from 'express';
import argon2 from 'argon2';
import debug from 'debug';
import { ServiceManager } from "../services/manager.service";
import { EServices } from "../enum/services.enum";
import { appLogger, errorLogger } from "../helper/logger.helper";
import { sendSomethingWrongMessage } from "../helper/response.helper";
import { EPermissionLevel } from "../../common/enum/permission.enum";
import { EHttpStatusCode } from "../../common/enum/http.enum";
import omit from "lodash/omit";

const log: debug.IDebugger = appLogger.extend('Users-controller');

const listUsers = async (req: express.Request, res: express.Response) => {
    const users = await ServiceManager
        .getService(EServices.UserService)
        .list(100, 0);
    res.status(EHttpStatusCode.success).send(users);
};

const getUserById = async (req: express.Request, res: express.Response) => {
    const user = await ServiceManager
        .getService(EServices.UserService)
        .readById(req.params.userId);
    res.status(EHttpStatusCode.success).send(user);
};

const createUser = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    req.body.password = await argon2.hash(req.body.password);
    const userService = ServiceManager.getService(EServices.UserService)
    const jwtService = ServiceManager.getService(EServices.JWTService)
    const user = await userService.create({ ...req.body, permissionLevel: EPermissionLevel.ADMIN });

    if (user) {
        const userId = user.id;
        try {
            const credentials = await ServiceManager
                .getService(EServices.AuthService)
                .addCredentials({ userId, ...jwtService.createToken({ userId }) });

            res
                .status(EHttpStatusCode.resourceCreationSuccess)
                .send({ ...user, ...omit(credentials, '_id', 'userId') });
        } catch (e) {
            errorLogger(e);
            sendSomethingWrongMessage(next)
        }
    } else {
        errorLogger('User creation is failed!!!');
        sendSomethingWrongMessage(next)
    }
};

const patchUser = async (req: express.Request, res: express.Response) => {
    if(req.body.password){
        req.body.password = await argon2.hash(req.body.password);
    }
    log(await ServiceManager
        .getService(EServices.UserService)
        .patchById(req.params.userId, req.body));
    res.status(204).send(``);
};

const putUser = async (req: express.Request, res: express.Response) => {
    req.body.password = await argon2.hash(req.body.password);
    log(await ServiceManager
        .getService(EServices.UserService)
        .updateById(req.params.userId, req.body));
    res.status(204).send(``);
};

const removeUser = async (req: express.Request, res: express.Response) => {
    log(await ServiceManager
        .getService(EServices.UserService)
        .deleteById(req.params.userId));
    res.status(204).send(``);
};

export {
    listUsers,
    getUserById,
    createUser,
    patchUser,
    putUser,
    removeUser,
};