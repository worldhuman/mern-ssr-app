import React from "react";
import express from 'express';
import { renderToStaticMarkup, renderToString } from "react-dom/server";
import { StaticRouter } from "react-router";
import { HtmlComponent } from "client/components/html/html.component";
import { CONSTANT_MAIN} from "../../common/constant/main.constant";
import { getRoutes, isSystemPage } from "../../common/helper/router.helper";
import { I18nextProvider } from "react-i18next";
import { Store } from "../../common/store";

const index = async (req: express.Request, res: express.Response) => {
  const scripts = ['client.js'];
  const styles = ['app.css'];

  const appMarkup = renderToString(
    <StaticRouter location={req.url} context={{}}>
      <I18nextProvider i18n={req.i18n}>
        { getRoutes(isSystemPage(req.path)) }
      </I18nextProvider>
    </StaticRouter>
  );

  const html = renderToStaticMarkup(
    <HtmlComponent
      children={appMarkup}
      scripts={scripts}
      styles={styles}
      initialState={new Store().data}
    />
  );

  res.setHeader("Content-Security-Policy", `script-src 'nonce-${CONSTANT_MAIN.SCRIPT_SRC_NONCE_KEY}'`);
  res.send(`<!doctype html>${html}`);
}

export {
    index,
};
