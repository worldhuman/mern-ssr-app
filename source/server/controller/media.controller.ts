import express from "express";
import {EHttpStatusCode} from "common/enum/http.enum";
import {mediaFileTransformer} from "server/transformers/file.transformer";
import {sendSomethingWrongMessage} from "../helper/response.helper";
import {ServiceManager} from "../services/manager.service";
import {EServices} from "../enum/services.enum";


const create = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
	try {
		const result = await ServiceManager
			.getService(EServices.MediaService)
			.create(req.file);
		res
			.status(EHttpStatusCode.success)
			.send({ result: mediaFileTransformer(result) });
	} catch (e) {
		sendSomethingWrongMessage(next);
	}
};

const getMediaById = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
	try {
		const result = await ServiceManager
			.getService(EServices.MediaService)
			.readById(req.params.id);
		res
			.status(EHttpStatusCode.success)
			.send({ result: mediaFileTransformer(result) });
	} catch {
		sendSomethingWrongMessage(next);
	}
}

const getMediaList = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
	try {
		const result = await ServiceManager
			.getService(EServices.MediaService)
			.list(20, 0);
		res
			.status(EHttpStatusCode.success)
			.send({ result: mediaFileTransformer(result) });
	} catch {
		sendSomethingWrongMessage(next);
	}
}

const deleteMediaById = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
	try {
		const result = await ServiceManager
			.getService(EServices.MediaService)
			.deleteById(req.params.id);
		res
			.status(EHttpStatusCode.success)
			.send({ result });
	} catch {
		sendSomethingWrongMessage(next);
	}
}

export {
	create,
	getMediaById,
	getMediaList,
	deleteMediaById,
};
