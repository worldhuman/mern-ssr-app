import express from "express";
import omit from "lodash/omit";
import { ServiceManager } from "../services/manager.service";
import { EServices } from "../enum/services.enum";
import { EHttpStatusCode } from "../../common/enum/http.enum";
import { sendSomethingWrongMessage } from "../helper/response.helper";

const exchangeToken = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
        const userId = req.body.id;
        if (userId) {
            const authService = await ServiceManager.getService(EServices.AuthService);
            const jwtService = ServiceManager.getService(EServices.JWTService);
            const accessToken = req.header('authorization')?.split(' ')?.[1];
            const credentials = await ServiceManager
                .getService(EServices.AuthService)
                .addCredentials({ userId, ...jwtService.createToken({ userId }) });
            await authService.dropCredentials({ userId, accessToken });
            res
                .status(EHttpStatusCode.success)
                .send(credentials);
        } else {
            next({ status: EHttpStatusCode.badRequest, message: 'user not found' })
        }
    } catch (err) {
        return next({ status: EHttpStatusCode.internalError, message: err });
    }
}

const login = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
        const {userId} = req.body;
        const jwtService = ServiceManager.getService(EServices.JWTService);
        const credentials = await ServiceManager
            .getService(EServices.AuthService)
            .addCredentials({ userId, ...jwtService.createToken({ userId }) });
        const user = await ServiceManager
            .getService(EServices.UserService)
            .readById(userId);
        res
            .status(EHttpStatusCode.success)
            .send({ ...user, ...omit(credentials, '_id', 'userId') });
    } catch (err) {
        return next({ status: EHttpStatusCode.internalError, message: err });
    }
}

const signout = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const authService = await ServiceManager.getService(EServices.AuthService);
    const accessToken = req.header('authorization')?.split(' ')?.[1];
    const userId = res.locals.jwt.id;
    try {
        const result = await authService.dropCredentials({ userId, accessToken });
        return res.status(EHttpStatusCode.success).send({ result });
    } catch (e) {
        return sendSomethingWrongMessage(next);
    }
}

export {
    exchangeToken,
    login,
    signout,
};
