export interface ICRUD {
    list: (limit: number, page: number) => Promise<any>;
    create: (resource: any) => Promise<any>;
    updateById: (id: string, resource: any) => Promise<string>;
    readById: (resourceId: any) => Promise<any>;
    deleteById: (resourceId: any) => Promise<boolean>;
    patchById: (id: string, resource: any) => Promise<string>;
}