export interface IAppService {
    start: () => void;
}