import {EPermissionLevel} from "../../common/enum/permission.enum";

export interface UserDto {
   id: string;
   email: string;
   password: string;
   firstName: string;
   lastName: string;
   permissionLevel: EPermissionLevel;
}