export interface MediaDto {
   filename: string;
   mimetype: string;
   size: string;
}