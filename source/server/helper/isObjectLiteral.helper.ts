const isObjectLiteralHelper = (obj: any) => obj && obj.constructor === Object;

export {
	isObjectLiteralHelper
}