const configurePathsHelper = ({rootPath, publicPath}: {rootPath: string; publicPath: string}) => {
	global.ROOT_PATH = rootPath;
	global.PUBLIC_PATH = publicPath;
};

export {
	configurePathsHelper
}