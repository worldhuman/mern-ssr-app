import {isObjectLiteralHelper} from "server/helper/isObjectLiteral.helper";

const isEmptyObjectHelper = (obj: any) => isObjectLiteralHelper(obj) && Object.keys(obj).length === 0;

export {
	isEmptyObjectHelper
}