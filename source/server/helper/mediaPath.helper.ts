const createClientMediaPath = (path: string) => {
	return `${process.env.BASE_URL}/uploads/${path}`;
};

export {
	createClientMediaPath,
};