import {MongoClient, Db} from 'mongodb';
import {errorLogger, mongoDBServiceLogger} from "../helper/logger.helper";
import {IAppService} from "../interface/service.interface";

class MongoDBService implements IAppService {
    private static instance: MongoDBService;
    private retryCount = 0;
    private dbInstance: Db | null = null;

    public start() {
        this.connectWithRetry();
    }

    private static get dbUser() {
        return process.env.DB_USER;
    }

    private static get dbPswd() {
        return process.env.DB_PSWD;
    }

    private static get dbName() {
        return process.env.DB_NAME;
    }

    private static get dbPort() {
        return process.env.DB_PORT;
    }

    public static getInstance() {
        if (!MongoDBService.instance) {
            MongoDBService.instance = new MongoDBService();
        }
        return MongoDBService.instance;
    }

    private get url() {
        return `mongodb://${MongoDBService.dbUser}:${MongoDBService.dbPswd}@localhost:${MongoDBService.dbPort}/${MongoDBService.dbName}?authSource=admin`;
    }

    public getDB(): Promise<Db> {
        return new Promise<Db>((resolve, reject) =>
            this.dbInstance ? resolve(this.dbInstance) : reject(null)
        )
    }

    public connectWithRetry = () => {
        mongoDBServiceLogger('MongoDB connection with retry', this.url);
        MongoClient
            .connect(this.url, { useUnifiedTopology: true } )
            .then(async (client: MongoClient) => {
                this.dbInstance = client.db();
                mongoDBServiceLogger('MongoDB is connected');
            })
            .catch(err => {
                errorLogger(err);
                mongoDBServiceLogger(`MongoDB connection unsuccessful, retry after 5 seconds. ${++this.retryCount}`);
                setTimeout(this.connectWithRetry, 5000)
            });
    };
}
export {
    MongoDBService,
};