import mongoose from 'mongoose';
import debug from 'debug';
import {IAppService} from "../interface/service.interface";

const log: debug.IDebugger = debug('app:mongoose-service');

class MongooseService implements IAppService {
    private static instance: MongooseService;
    private count = 0;
    private mongooseOptions = {useNewUrlParser: true, useUnifiedTopology: true, serverSelectionTimeoutMS: 5000};

    public constructor() {
        this.connectWithRetry();
    }

    public start(){};

    public static getInstance() {
        if (!this.instance) {
            this.instance = new MongooseService();
        }
        return this.instance;
    }

    public getMongoose() {
        return mongoose;
    }

    connectWithRetry = () => {
        log('MongoDB connection with retry');
        mongoose.connect("mongodb://localhost:27017/api-db", this.mongooseOptions).then(() => {
            log('MongoDB is connected')
        }).catch(err => {
            log(`MongoDB connection unsuccessful, retry after 5 seconds. ${++this.count}`);
            setTimeout(this.connectWithRetry, 5000)
        })
    };
}

export default MongooseService.getInstance();