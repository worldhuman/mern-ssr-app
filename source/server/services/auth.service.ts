import { IAppService } from "../interface/service.interface";
import AuthDal from '../dal/auth.dal';

class AuthService implements IAppService {
    private static instance: AuthService;

    public static getInstance(): AuthService {
        if (!AuthService.instance) {
            AuthService.instance = new AuthService();
        }
        return AuthService.instance;
    }

    public start() {}

    public async addCredentials(data: { userId: string, accessToken: string, refreshToken: string }) {
        return await AuthDal.addCredentials(data)
    }

    public async dropCredentials(data: { userId: string, accessToken: string }) {
        return await AuthDal.dropCredentials(data)
    }

    public async getUserByIdAndAccessToken(data: { userId: string, accessToken: string }) {
        return await AuthDal.getUserByIdAndAccessToken(data)
    }
}

export {
    AuthService,
};