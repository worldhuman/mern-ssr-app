import MediaDal from '../dal/media.dal';
import {ICRUD} from "../interface/crud.interface";
import {IAppService} from "../interface/service.interface";
import {MediaDto} from "../dto/media.model";

class MediaService implements ICRUD, IAppService {
    private static instance: MediaService;

    public static getInstance(): MediaService {
        if (!MediaService.instance) {
            MediaService.instance = new MediaService();
        }
        return MediaService.instance;
    }

    public start() {}

    public async create(file: any) {
        return MediaDal.addMedia(file);
    }

    public async deleteById(id: string) {
        return await MediaDal.deleteById(id);
    };

    public async list(limit?: number, page?: number) {
        return MediaDal.getList(limit, page);
    };

    public async patchById(id: string, data: MediaDto): Promise<any> {
        return null
    };

    public async readById(id: string) {
        return MediaDal.getById(id);;
    };

    public async updateById(id: string, data: MediaDto): Promise<any> {
        return null;
    };
}

export {
    MediaService,
};