import crypto from "crypto";
import jwt, {TokenExpiredError} from "jsonwebtoken";
import { errorLogger } from "../helper/logger.helper";
import { IAppService } from "../interface/service.interface";
import { SERVER_CONSTANT } from "../constant/srver.constant";
import { EHttpStatusCode } from "../../common/enum/http.enum";
import { EApiErrors } from "../../common/enum/apiErrors.enum";

class JWTService implements IAppService {
    private static instance: JWTService;

    public static getInstance(): JWTService {
        if (!JWTService.instance) {
            JWTService.instance = new JWTService();
        }
        return JWTService.instance;
    }

    public start() {}

    public createToken({ userId }: {userId: string}) {
        const salt = crypto.randomBytes(16).toString('base64');;
        const refreshKey = salt;
        const accessToken = jwt.sign(
            { id: userId, type: "access", refreshKey },
            process.env.JWT_SECRET as string,
            { expiresIn: SERVER_CONSTANT.ACCESS_TOKEN_EXP_IN_SECONDS }
            );
        const hash = crypto
            .createHmac('sha512', refreshKey)
            .update(userId + process.env.JWT_SECRET)
            .digest("base64");

        const key = Buffer.from(hash).toString('base64');
        const refreshToken = jwt.sign(
            { id: userId, key, type: "refresh" },
            process.env.JWT_SECRET as string,
            { expiresIn: SERVER_CONSTANT.REFRESH_TOKEN_EXP_IN_SECONDS }
            );

        return {accessToken, refreshToken};
    }

    public verifyAccessToken({ accessToken }: { accessToken: string }) {
        return new Promise((resolve) => {
            try {
                jwt.verify(accessToken, process.env.JWT_SECRET as string, (err: any, decoded: any) => {
                    if (err) {
                        return resolve({
                            error: {
                                status: EHttpStatusCode.unAuthorized,
                                message: err instanceof TokenExpiredError ? EApiErrors.tokenExpired : err?.message
                            }
                        });
                    }

                    resolve({ decoded, error: null })
                });

            } catch (err) {
                return resolve({
                    error: {
                        status: EHttpStatusCode.unAuthorized,
                        message: err?.message
                    }
                });
            }
        });
    }

    public verifyRefreshToken({userId, refreshToken, jwtRefreshKey}: {userId: string, refreshToken: string, jwtRefreshKey: string}) {
        return new Promise((resolve) => {
            try {
                jwt.verify(refreshToken, process.env.JWT_SECRET as string, (err: any, decoded: any) => {
                    if (err) {
                        return resolve({
                            error: {
                                status: EHttpStatusCode.unAuthorized,
                                message: err instanceof TokenExpiredError ? EApiErrors.refreshTokenExpired : err?.message
                            }
                        });
                    }

                    const refreshTokenKey = decoded?.key;
                    const hash = crypto
                        .createHmac('sha512', jwtRefreshKey)
                        .update(userId + process.env.JWT_SECRET)
                        .digest("base64");
                    const key = Buffer.from(hash).toString('base64');

                    if (key === refreshTokenKey) {
                        resolve({ decoded, error: null });
                    } else {
                        resolve({ decoded, error: { status: EHttpStatusCode.badRequest, message: EApiErrors.invalidRefreshToken } });
                    }
                });
            } catch (err) {
                errorLogger(err);
                resolve({ error: { status: EHttpStatusCode.badRequest, message: err.message } });
            }
        });
    }
}

export {
    JWTService,
};