import UsersDal from '../dal/user.dal';
import { ICRUD } from "../interface/crud.interface";
import { UserDto } from "../dto/user.model";
import { IAppService } from "../interface/service.interface";

class UserService implements ICRUD, IAppService {
    private static instance: UserService;

    public static getInstance(): UserService {
        if (!UserService.instance) {
            UserService.instance = new UserService();
        }
        return UserService.instance;
    }

    public start() {}

    public async create(user: UserDto) {
        return UsersDal.addUser(user);
    }

    public async deleteById(userId: string) {
        return UsersDal.removeUserById(userId);
    };

    public async list(limit: number, page: number) {
        return UsersDal.getUsers(limit, page);
    };

    public async patchById(userId: string, user: UserDto): Promise<any> {
        return UsersDal.patchUserById(userId, user)
    };

    public async readById(userId: string) {
        return await UsersDal.getUserById(userId);
    };

    public async updateById(userId: string, user: UserDto): Promise<any> {
        return UsersDal.patchUserById(userId, user);
    };

    public async getUserByEmail(email: string) {
        return UsersDal.getUserByEmail(email);
    }

    public async getUserByEmailWithPassword(email: string) {
        return UsersDal.getUserByEmailWithPassword(email);
    }
}

export {
    UserService,
};