class Manager {
	private services: {[key: string]: any} = {};

	public registerServices(appServices: { [key: string]: {serviceClass: any; isAutoStart: boolean} }) {
		for (const [serviceName, ServiceOpts] of Object.entries(appServices)) {
			const {serviceClass, isAutoStart} = ServiceOpts;
			this.services[serviceName] = serviceClass.getInstance();
			this.services[serviceName].isAutoStart = isAutoStart;
		}
		return this;
	}

	public async startServices() {
		for (const serviceName in this.services) {
			const service = this.services[serviceName];
			if (service.isAutoStart) {
				await service.start();
			}
		}
		return this;
	}

	public hasService(serviceName: string) {
		return !!this.services[serviceName];
	}

	public getService(serviceName: string) {
		return this.services[serviceName];
	}
}

export const ServiceManager = new Manager();
