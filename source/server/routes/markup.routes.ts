import express from "express";
import {RouterConfig} from "./config/router.config";
import * as MarkupController from "../controller/markup.controller";
import * as authMiddleware from "../middleware/auth.middleware";
import * as pageEffectsMiddleware from "../middleware/pageEffects.middleware";
import { i18nextHttpMiddleware } from "../middleware/i18next.middleware";

export class MarkupRoutes extends RouterConfig {
    constructor(app: express.Application) {
        super(app, 'MarkupRoutes');
    }

    configureRoutes() {
        this.app.get(`*`, [
            authMiddleware.checkSystemPageAvailability,
            i18nextHttpMiddleware,
            pageEffectsMiddleware.invokeEffects,
            MarkupController.index
        ]);
        return this.app;
    }
}