import express from 'express';
import {RouterConfig} from './config/router.config';
import * as MediaController from '../controller/media.controller';
import * as MulterMiddleware from '../middleware/multer.middleware';
import * as MediaMiddleware from '../middleware/media.middleware';
import * as jwtMiddleware from "../middleware/jwt.middleware";
import * as permissionMiddleware from "../middleware/permission.middleware";

export class MediaRoutes extends RouterConfig {
	constructor(app: express.Application) {
		super(app, 'MediaRoutes');
	}

	configureRoutes() {
		this.app
			.get(`/media`, MediaController.getMediaList)
			.get(`/media/:id`, MediaController.getMediaById)
			.post(`/media/upload/image`,[
				// jwtMiddleware.validJWTNeeded,
				// permissionMiddleware.onlyAdminCanDoThisAction,
				MulterMiddleware.upload.single('file'),
				MediaMiddleware.validateRequestPayloadFileExists,
				MediaController.create
			])
			.delete(`/media/:id`, [
				jwtMiddleware.validJWTNeeded,
				permissionMiddleware.onlyAdminCanDoThisAction,
				MediaController.deleteMediaById
			]);
		return this.app;
	}
}