import {RouterConfig} from './config/router.config';
import * as UsersController from '../controller/users.controller';
import * as UsersMiddleware from '../middleware/user.middleware';
import express from 'express';
import * as jwtMiddleware from '../middleware/jwt.middleware';
import * as permissionMiddleware from '../middleware/permission.middleware';
import {EPermissionLevel} from 'common/enum/permission.enum';

export class UsersRoutes extends RouterConfig {
	constructor(app: express.Application) {
		super(app, 'UsersRoutes');
	}

	configureRoutes() {
		this.app.route(`/signup`)
			.post(
				UsersMiddleware.validateRequiredUserBodyFieldsForSignup,
				UsersMiddleware.validateSameEmailDoesntExist,
				UsersController.createUser);
		this.app.route(`/users`)
			.get(
				jwtMiddleware.validJWTNeeded,
                permissionMiddleware.onlyAdminCanDoThisAction,
                UsersController.listUsers);

		this.app.param(`userId`, UsersMiddleware.extractUserId);
		this.app.route(`/users/:userId`)
			.all(
				UsersMiddleware.validateUserExists,
                jwtMiddleware.validJWTNeeded,
                permissionMiddleware.onlySameUserOrAdminCanDoThisAction)
			.get(UsersController.getUserById)
			.delete(UsersController.removeUser);

		this.app.put(`/users/:userId`, [
            UsersMiddleware.validateRequiredUserBodyFields,
            UsersMiddleware.validateSameEmailBelongToSameUser,
            jwtMiddleware.validJWTNeeded,
            permissionMiddleware.onlySameUserOrAdminCanDoThisAction,
            permissionMiddleware.minimumPermissionLevelRequired(EPermissionLevel.ADMIN),
            UsersController.putUser
        ]);

		this.app.patch(`/users/:userId`, [
            UsersMiddleware.validatePatchUserEmail,
            jwtMiddleware.validJWTNeeded,
            permissionMiddleware.onlySameUserOrAdminCanDoThisAction,
            permissionMiddleware.minimumPermissionLevelRequired(EPermissionLevel.ADMIN),
            UsersController.patchUser
        ]);

		return this.app;
	}
}