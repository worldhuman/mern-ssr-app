import express from 'express';
import { RouterConfig } from './config/router.config';
import * as jwtMiddleware from '../middleware/jwt.middleware';
import * as authMiddleware from '../middleware/auth.middleware';
import * as authController from '../controller/auth.controller';

export class AuthRoutes extends RouterConfig {
    constructor(app: express.Application) {
        super(app, 'AuthRoutes');
    }

    configureRoutes() {
        this
            .app
            .post(`/auth`, [
                authMiddleware.verifyNotAuthorized,
                authMiddleware.validateBodyRequest,
                authMiddleware.verifyUserPassword,
                authController.login
            ])
            .post(`/auth/token/exchange`, [
                jwtMiddleware.validJWTNeeded,
                jwtMiddleware.verifyRefreshBodyField,
                jwtMiddleware.validRefreshNeeded,
                authMiddleware.verifyIsAuthorized,
                authController.exchangeToken
            ])
            .post(`/auth/signout`, [
                jwtMiddleware.validJWTNeeded,
                authMiddleware.verifyIsAuthorized,
                authController.signout
            ]);
        return this.app;
    }
}