import express from "express";
import {RouterConfig} from "./config/router.config";
import {MediaRoutes} from './media.routes';
import {UsersRoutes} from "server/routes/user.routes";
import {AuthRoutes} from 'server/routes/auth.routes';
import {MarkupRoutes} from "./markup.routes";

const routes: Array<RouterConfig> = [];

const configureRoutes = (app: express.Application) => {
	routes.push(new UsersRoutes(app));
	routes.push(new AuthRoutes(app));
	routes.push(new MediaRoutes(app));
	routes.push(new MarkupRoutes(app));
};

export {
	routes,
	configureRoutes,
};