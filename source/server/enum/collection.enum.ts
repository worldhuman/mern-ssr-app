enum ECollections {
	media = 'media',
	users = 'users',
	auth = 'auth',
}

export {
	ECollections,
};