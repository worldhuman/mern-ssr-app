enum EServices {
	MediaService = "MediaService",
	MongooseService = 'MongooseService',
	UserService = 'UserService',
	MongoDBService = 'MongoDBService',
	JWTService = 'JWTService',
	AuthService = 'AuthService',
}

export {
	EServices,
};
