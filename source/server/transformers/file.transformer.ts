import {isObjectLiteralHelper} from "server/helper/isObjectLiteral.helper";
import {isEmptyObjectHelper} from "server/helper/isEmptyObject.helper";

const fractal = require("fractal-transformer")();

const schema = {
	_id: "_id",
	size: 'size',
	uri: 'uri',
};


const mediaFileTransformer = (data: any[] | {[key: string]: any}) => {
	if (Array.isArray(data) && !!data.length) {
		return fractal(data, schema);
	}

	if (isObjectLiteralHelper(data) && !isEmptyObjectHelper(data) || (Array.isArray(data) && !!data.length)) {
		return fractal(data, schema);
	}

	return data || null;
};


export {
	mediaFileTransformer,
};