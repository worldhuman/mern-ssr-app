import express from "express";
import bodyParser from "body-parser";
import path from "path";
import helmet from "helmet";
import cors from "cors";
import favicon from "serve-favicon";
import cookieParser from "cookie-parser";
import { configureRoutes } from "./routes";
import { configurePathsHelper } from "./helper/configurePaths.helper";
import { loggerMiddleware, errorLogger } from "server/middleware/logger.middleware";
import { clientErrorHandlerMiddleware } from "./middleware/clientErrorHandler.middleware";

const app: express.Application = express();

configurePathsHelper({
	rootPath: path.resolve(__dirname, '../'),
	publicPath:  path.resolve(__dirname, '../public')
});

app.use(cookieParser());

app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({
	limit: "50mb",
	extended: true,
	parameterLimit: 50000
}));

app.use(cors());

app.use(favicon(path.join(global.PUBLIC_PATH, 'favicon.ico')));

app.use(express.static(global.PUBLIC_PATH));

loggerMiddleware(app);

configureRoutes(app);

errorLogger(app);

app.use(clientErrorHandlerMiddleware);

app.use(helmet({
	contentSecurityPolicy: false,
}));

export {
	app
};