import { EServices } from "../../enum/services.enum";
import { MediaService } from "../../services/media.service";
import { MongoDBService } from "../../services/mongodb.service";
import { UserService } from "../../services/user.service";
import { JWTService } from "../../services/jwt.service";
import {AuthService} from "../../services/auth.service";

const APP_SERVICES = {
	[EServices.MediaService]: {
		serviceClass: MediaService,
		isAutoStart: true
	},
	[EServices.MongoDBService]: {
		serviceClass: MongoDBService,
		isAutoStart: true
	},
	[EServices.UserService]: {
		serviceClass: UserService,
		isAutoStart: true
	},
	[EServices.JWTService]: {
		serviceClass: JWTService,
		isAutoStart: true
	},
	[EServices.AuthService]: {
		serviceClass: AuthService,
		isAutoStart: true
	},
};

export {
	APP_SERVICES,
};