import i18next from 'i18next';
import middleware from 'i18next-http-middleware';
import { i18nextOptions } from "../../common/localisation/config";

i18next
  .use(middleware.LanguageDetector)
  .init(i18nextOptions);

const i18nextHttpMiddleware = middleware.handle(i18next);

export {
  i18nextHttpMiddleware,
}