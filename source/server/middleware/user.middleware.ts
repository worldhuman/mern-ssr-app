import express from 'express';
import pick from "lodash/pick";
import { ServiceManager } from "../services/manager.service";
import { EServices } from "../enum/services.enum";
import { EHttpStatusCode } from "../../common/enum/http.enum";

const validateRequiredUserBodyFieldsForSignup = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const userFieldsKeys = ['firstName', 'lastName', 'email', 'password'];
    const userFields = pick((req.body || {}), ...userFieldsKeys);
    if (req.body && userFieldsKeys.every((key) => typeof userFields[key] !== 'undefined')) {
        req.body = userFields;
        next();
    } else {
        next({status: EHttpStatusCode.badRequest, message: `Missing required fields. Required fields are ${userFieldsKeys.join(', ')}`});
    }
};

const validateRequiredUserBodyFields = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (req.body && req.body.email && req.body.password) {
        next();
    } else {
        next({ status: EHttpStatusCode.badRequest, message: `Missing required fields email and password` });
    }
};

const validateSameEmailDoesntExist = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const user = await ServiceManager
        .getService(EServices.UserService)
        .getUserByEmail(req.body.email);

    if (user) {
        next({ status: EHttpStatusCode.badRequest, message: `User email already exists` });
    } else {
        next();
    }
};

const validateSameEmailBelongToSameUser = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const user = await ServiceManager
        .getService(EServices.UserService)
        .getUserByEmail(req.body.email);
    if (user && user.id === req.params.userId) {
        next();
    } else {
        next({ status: EHttpStatusCode.badRequest, message: `Invalid email` });
    }
};

const validatePatchUserEmail = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (req.body.email) {
        validateSameEmailBelongToSameUser(req, res, next);
    } else {
        next();
    }
};

const validateUserExists = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const user = await ServiceManager
        .getService(EServices.UserService)
        .readById(req.params.userId);
    if (user) {
        next();
    } else {
        next({ status: EHttpStatusCode.notFound, message: `User ${req.params.userId} not found` });
    }
};

const extractUserId = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    req.body.id = req.params.userId;
    next();
};

export {
    validateRequiredUserBodyFields,
    validateSameEmailDoesntExist,
    validateSameEmailBelongToSameUser,
    validatePatchUserEmail,
    validateUserExists,
    extractUserId,
    validateRequiredUserBodyFieldsForSignup,
};