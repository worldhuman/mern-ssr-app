import { EHttpStatusCode } from "common/enum/http.enum";
import { MulterError } from "multer";
import express from "express";

const clientErrorHandlerMiddleware = (error: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
	let status = error.status || EHttpStatusCode.internalError;
	let message = error.message;

	console.error(error.message);

	if (error) {

		if (error instanceof MulterError) {

			status = EHttpStatusCode.badRequest;
			message = `${error.message}: '${error.field}'`;

		} else if (error.headers) {

			for (const [headerName, headerValue] of Object.entries(error.headers)) {
				res.setHeader(headerName, headerValue as string);
			}

			status = error.status;
		}

		res.status(status).send({ error: message })
	} else {
		next();
	}
};

export {
	clientErrorHandlerMiddleware,
};