import fs from "fs";
import path from "path";
import multer from "multer";
import {EHttpStatusCode} from "common/enum/http.enum";
import {CONSTANT_MAIN} from "common/constant/main.constant";
import {makeHashString} from "server/helper/makeHash.helper";
import express from "express";

const destination = (req: express.Request, file: any, cb: any) => {
	const dir = path.join(global.PUBLIC_PATH, 'uploads');

	if (!fs.existsSync(dir)){
		fs.mkdirSync(dir);
	}

	cb(null, dir);
};

const filename = (req: express.Request, file: any, cb: any) => {
	const ext = path.extname(file.originalname).toLowerCase();
	cb(null , `${makeHashString(24)}${ext}`);
};

const fileFilter = (req: express.Request, file: any, cb: any) => {
	let isAllowed = true;
	if (!CONSTANT_MAIN.supportedImgMimeTypes.includes(file.mimetype)) {
		isAllowed = false;
		cb({status: EHttpStatusCode.badRequest, message: `File type doesn't supported '${file.mimetype}': Allowed types - ${CONSTANT_MAIN.supportedImgMimeTypes.join(', ')}:`});
	}
	cb(null, isAllowed);
};

const storage = multer.diskStorage({
	destination,
	filename
});

const upload = multer({ fileFilter, storage });

export {
	upload,
}