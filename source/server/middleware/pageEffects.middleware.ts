import express from 'express';
import { getRouteConfig } from "../../client/routes/config";

const invokeEffects = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const config = getRouteConfig(req.path, ['init']);

    try {
        if (config?.init) {
            await config?.init();
        }
    } finally {
        next();
    }
};

export {
    invokeEffects,
};