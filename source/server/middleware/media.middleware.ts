import {EHttpStatusCode} from "common/enum/http.enum";
import express from "express";

const validateRequestPayloadFileExists = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (req.file) {
        next();
    } else {
        return next({status: EHttpStatusCode.badRequest, message: 'Upload file is missing'})
    }
};

export {
    validateRequestPayloadFileExists,
};