import express from 'express';
import * as argon2 from 'argon2';
import { ServiceManager } from "../services/manager.service";
import { EServices } from "../enum/services.enum";
import { EHttpStatusCode } from "../../common/enum/http.enum";
import { EApiErrors } from "../../common/enum/apiErrors.enum";
import { SYSTEM_ROUTES } from "../../common/constant/appRoutes.constant";

const validateBodyRequest = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (req.body && req.body.email && req.body.password) {
        next();
    } else {
        res.status(400).send({error: 'Missing body fields: email, password'});
    }
}

const verifyUserPassword = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const user: any = await ServiceManager
        .getService(EServices.UserService)
        .getUserByEmailWithPassword(req.body.email);
    if (user) {
        let passwordHash = user.password;
        if (await argon2.verify(passwordHash, req.body.password)) {
            req.body.userId = user._id;
            return next();
        } else {
            next({ status: EHttpStatusCode.badRequest, message: `Invalid email and/or password` });
        }
    } else {
        next({ status: EHttpStatusCode.badRequest, message: `User not found by email `});
    }
}

const verifyNotAuthorized = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const authorization = req.headers['authorization']?.split(' ');
    if (authorization) {
        next({ status: EHttpStatusCode.badRequest, message: EApiErrors.shouldBeUnAuthorized });
    } else {
        next();
    }
}

const verifyIsAuthorized = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const accessToken = req.headers['authorization']?.split(' ')?.[1];
    const userId = res.locals.jwt.id;

    if (userId && accessToken) {
        const user = await ServiceManager
            .getService(EServices.AuthService)
            .getUserByIdAndAccessToken({ userId, accessToken });
        return user ? next() : next({ status: EHttpStatusCode.unAuthorized });
    }

    return next({ status: EHttpStatusCode.unAuthorized });
}

const checkSystemPageAvailability = async (req: express.Request, res: express.Response, next: express.NextFunction) => {

    if (!Object.values(SYSTEM_ROUTES).includes(req.path)) {
        return next();
    }

    const moveToLogin = () => {
        res.clearCookie("authorization");
        return res.redirect(
            EHttpStatusCode.movePermanently,
            SYSTEM_ROUTES.LOGIN
        );
    };

    const moveToSystem = () => res.redirect(
        EHttpStatusCode.movePermanently,
        SYSTEM_ROUTES.SYSTEM
    );

    const isLoginPage = req.path === SYSTEM_ROUTES.LOGIN;
    const accessToken = (req.headers.authorization || req.cookies.authorization)?.split(' ')?.[1];

    if (!accessToken) {
        return isLoginPage ? next() : moveToLogin();
    }

    const { error, decoded } = await ServiceManager
        .getService(EServices.JWTService)
        .verifyAccessToken({ accessToken });

    if (error) {
        return moveToLogin();
    }

    const user = await ServiceManager
        .getService(EServices.AuthService)
        .getUserByIdAndAccessToken({ userId: decoded.id, accessToken });

    return user
        ? (isLoginPage ? moveToSystem() : next())
        : moveToLogin();
};

export {
    validateBodyRequest,
    verifyUserPassword,
    verifyIsAuthorized,
    verifyNotAuthorized,
    checkSystemPageAvailability,
};