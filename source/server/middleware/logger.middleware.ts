import winston from "winston";
import expressWinston from "express-winston";
import express from "express";

const loggerMiddleware = (app: express.Application) => {
	app.use(expressWinston.logger({
		transports: [
			new winston.transports.Console()
		],
		format: winston.format.combine(
			winston.format.colorize(),
			winston.format.json()
		),
		meta: false, // optional: control whether you want to log the meta data about the request (default to true)
		msg: "HTTP {{req.method}} {{req.url}} {{res.statusCode}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
		expressFormat: false, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
		colorize: true, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
		ignoreRoute: (req: any, res: any) => false // optional: allows to skip some log messages based on request and/or response
	}));
};

const errorLogger = (app: express.Application) => {
	const options = {
		file: {
			level: 'info',
			filename: `${global.ROOT_PATH}/logs/app.log`,
			handleExceptions: true,
			json: true,
			maxsize: 1048576, // 1MB
			maxFiles: 5,
			colorize: false,
		},
		console: {
			level: 'debug',
			handleExceptions: true,
			json: false,
			colorize: true,
		},
	};
	app.use(expressWinston.errorLogger({
		transports: [
			new winston.transports.File(options.file),
			new winston.transports.Console(options.console)
		],
		format: winston.format.combine(
			winston.format.colorize(),
			winston.format.json()
		),
	}));
};

export {
	loggerMiddleware,
	errorLogger,
};