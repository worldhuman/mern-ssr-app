import express from 'express';
import { EServices } from "../enum/services.enum";
import { ServiceManager } from "../services/manager.service";
import { EHttpStatusCode } from "../../common/enum/http.enum";

const verifyRefreshBodyField = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    return req.body && req.body.refreshToken
        ? next()
        : next({status: EHttpStatusCode.badRequest, message: 'need body field: refreshToken'});
};

const validRefreshNeeded = async (req: any, res: express.Response, next: express.NextFunction) => {
    const jwtObj = res.locals?.jwt || {};
    const jwtRefreshKey = jwtObj?.refreshKey;
    const { refreshToken } = req.body;

    const { error } = await ServiceManager
        .getService(EServices.JWTService)
        .verifyRefreshToken({ userId: jwtObj.id, refreshToken, jwtRefreshKey });

    if (error) {
        return next(error);
    } else {
        req.body = jwtObj;
        return next();
    }
};


const validJWTNeeded = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const authorization = req.headers['authorization']?.split(' ');

    if (!authorization?.length || authorization[0] !== 'Bearer') {
        return next({ status: EHttpStatusCode.unAuthorized });
    }

    const { error, decoded } = await ServiceManager
        .getService(EServices.JWTService)
        .verifyAccessToken({ accessToken: authorization[1] });

    if (error) {
        return next(error);
    } else {
        res.locals.jwt = decoded;
        return next();
    }
};

export {
    verifyRefreshBodyField,
    validRefreshNeeded,
    validJWTNeeded,
};