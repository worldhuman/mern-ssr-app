import { EPermissionLevel } from "common/enum/permission.enum";
import { EHttpStatusCode } from "../../common/enum/http.enum";
import { errorLogger } from "./logger.middleware";

const minimumPermissionLevelRequired = (requiredPermissionLevel: EPermissionLevel) => {
    return (req: any, res: any, next: any) => {
        try {
            let userPermissionLevel = parseInt(req.jwt.permissionLevel);
            if (userPermissionLevel & requiredPermissionLevel) {
                return next();
            } else {
                return next({ status: EHttpStatusCode.forbidden });
            }
        } catch (e) {
            errorLogger(e);
        }

    };
};

const onlySameUserOrAdminCanDoThisAction = async (req: any, res: any, next: any) => {
    let userPermissionLevel = parseInt(req.jwt.permissionLevel);
    let userId = req.jwt.userId;
    if (req.params && req.params.userId && userId === req.params.userId) {
        return next();
    } else {
        if (userPermissionLevel & EPermissionLevel.ADMIN) {
            return next();
        } else {
            return next({ status: EHttpStatusCode.forbidden });
        }
    }
};

const onlyAdminCanDoThisAction = async (req: any, res: any, next: any) => {
    let userPermissionLevel = parseInt(req.jwt.permissionLevel);
    if (userPermissionLevel & EPermissionLevel.ADMIN) {
        return next();
    } else {
        return next({ status: EHttpStatusCode.forbidden });
    }
};

export {
    onlyAdminCanDoThisAction,
    minimumPermissionLevelRequired,
    onlySameUserOrAdminCanDoThisAction,
};