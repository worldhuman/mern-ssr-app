const isServerSide = () => typeof window === 'undefined';

export {
  isServerSide,
};