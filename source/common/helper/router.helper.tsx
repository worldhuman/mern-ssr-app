import React from "react";
import { Route } from "react-router-dom";
import { adminRoutes, commonRoutes } from "../../client/routes";
import { APP_ROUTES, SYSTEM_ROUTES } from "../constant/appRoutes.constant";

const isSystemPage = (locationPathName: string) => Object.values(SYSTEM_ROUTES).includes(locationPathName);

const getRoutes = (isSystemPage: boolean) => (
	<Route path={ APP_ROUTES.ROOT } onChange={() => {debugger;}}>
		{ isSystemPage ? adminRoutes : commonRoutes	}
	</Route>
);

export {
	isSystemPage,
	getRoutes,
}