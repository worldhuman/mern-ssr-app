import { STORE_KEYS_BLOC_MAP } from "../constant/store.constant";

class Store {
  public data: any = {};

  constructor() {
    for (const [key, BlocClass] of Object.entries(STORE_KEYS_BLOC_MAP)) {
      this.data[key] = BlocClass.getData();
    }
  }
}

export {
  Store
};