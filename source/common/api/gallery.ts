import { request } from "../../client/helper/request";

const getGalleryImages = async () => {
  try {
    const response = await request('http://127.0.0.1/media');
    return response?.result;
  } catch (e) {
    throw e;
  }
};

export {
  getGalleryImages,
}