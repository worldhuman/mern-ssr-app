import en from "./data/en.json";
import fr from "./data/fr.json";
import ru from "./data/ru.json";
import hy from "./data/hy.json";
import { Lang } from "../enum/localisation.enum";

const detection = {
  order: [
    'cookie',
  ],

  lookupCookie: 'lang',
  caches: ['cookie'],
};

const i18nextOptions = {
  resources: {
    en,
    fr,
    ru,
    hy
  },
  fallbackLng: Lang.en,
  debug: process.env.NODE_ENV === 'development',
  detection,
  interpolation: {
  escapeValue: false, // not needed for react!!
    formatSeparator: ","
},

  react: {
    wait: true
  }
}

export {
  i18nextOptions,
};