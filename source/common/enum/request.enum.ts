enum ERequestMethod {
	get = 'get',
	post = 'post',
	put = 'put',
	delete = 'delete',
}

enum EResponseStatus {
	success = 'success',
	error = 'error',
}

enum EAsyncState {
	idle = 'idle',
	pending = 'pending',
	failure = 'failure',
}

enum EApiErrors {
	internal = 500,
}

export {
	ERequestMethod,
	EResponseStatus,
	EAsyncState,
	EApiErrors,
};