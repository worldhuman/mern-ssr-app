enum EHttpStatusCode {
	success = 200,
	resourceCreationSuccess = 201,
	movePermanently = 301,
	badRequest = 400,
	unAuthorized = 401,
	forbidden = 403,
	notFound = 404,
	internalError = 500,
}

export {
	EHttpStatusCode,
};