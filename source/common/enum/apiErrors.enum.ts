enum EApiErrors {
	tokenExpired = 'Access token expired',
	refreshTokenExpired = 'Refresh token expired',
	invalidRefreshToken = 'Invalid refresh token',
	shouldBeUnAuthorized = 'User should not be authorized',
}

export {
	EApiErrors,
};