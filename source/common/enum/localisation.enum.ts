enum Lang {
  en = 'en',
  hy = 'hy',
  fr = 'fr',
  ru = 'ru',
}

export {
  Lang,
}