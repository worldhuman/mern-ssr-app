const SYSTEM_ROUTES = {
	LOGIN: '/login',
	SYSTEM: '/system',
}

const APP_ROUTES = {
	ROOT: '/',
	BLUEBERRY: '/blueberry',
	RASPBERRY: '/raspberry',
	GALLERY: '/gallery',
	ABOUT: '/about',
	CONTACT: '/contact',
	NOMATCH: '*',
};


const API_ENDPOINTS = {
	AUTH: '/auth',
	AUTH_SIGNUP: '/auth/signup',
	AUTH_SIGNOUT: '/auth/signout',
	AUTH_TOKEN_EXCHANGE: '/auth/token/exchange',
};

export {
	APP_ROUTES,
	SYSTEM_ROUTES,
	API_ENDPOINTS,
};