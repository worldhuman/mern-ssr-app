const CONSTANT_MAIN = {
	API_URL: 'https://127.0.0.1',
	supportedImgMimeTypes: ['image/png', 'image/jpg', 'image/jpeg'],
	SCRIPT_SRC_NONCE_KEY: "01EZWPYGWY28NCA6DQMPGX0KPD",
};

export {
	CONSTANT_MAIN,
};