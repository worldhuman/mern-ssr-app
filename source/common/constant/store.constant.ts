import { GalleryBloc } from "../bloc";
import { StoreKeys } from "../enum/store.enum";

const STORE_KEYS_BLOC_MAP = {
  [StoreKeys.gallery]: GalleryBloc
};

export {
  STORE_KEYS_BLOC_MAP,
};