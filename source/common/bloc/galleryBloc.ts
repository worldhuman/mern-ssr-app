import { BehaviorSubject } from "rxjs";
import { api } from "../api";
import cloneDeep from 'lodash/cloneDeep';
export class Notification {
  constructor(public message: string | null, public shown: boolean) {}
}

class GalleryBloc {
  public notification = new BehaviorSubject(new Notification(null, false));
  protected initialData: any = {
    images: new BehaviorSubject([])
  };

  public data: any = cloneDeep(this.initialData);

  public getData() {
    const data: any = {};
    for (const item in this.data) {
      data[item] = this.data[item].value
    }

    this.resetData();

    return data;
  }

  public setData(data: any) {
    for (const item in data) {
      this.data[item].next(data[item])
    }
  }

  protected resetData() {
    this.data = cloneDeep(this.initialData);
  }

  public async start() {
    const result = await api.gallery.getGalleryImages();
    this.data.images.next(result);
  }
}

export default new GalleryBloc();