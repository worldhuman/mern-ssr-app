import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import { i18nextOptions } from "../../common/localisation/config";

i18n
  .use(LanguageDetector)
  .init(i18nextOptions);

export default i18n;