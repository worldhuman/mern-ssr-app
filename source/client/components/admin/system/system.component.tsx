import React from "react";

const SystemComponent = () => {
	return (
		<div>
			<h1>System Component</h1>
			<h2>This is the System page</h2>
		</div>
	)
};

export {
	SystemComponent,
};