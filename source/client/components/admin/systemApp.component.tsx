import React from 'react';
import Helmet from "react-helmet";

const SystemAppComponent = (props: { children?: React.ReactElement }) => {
	const { children } = props;
	return (
		<div>
			<Helmet>
				<title>Admin Page</title>
				<meta name="description" content="This is a proof of concept for React SSR" />
			</Helmet>
			{ children }
		</div>
	)
};

export {
	SystemAppComponent,
};