import { Link } from "react-router-dom";
import React from 'react';
import {HeaderComponent} from "./header/header.component";

const LayoutComponent = (props: { children?: React.ReactElement }) => {
	const { children } = props;
	return (
		<div>
			<HeaderComponent />
			{ children }
		</div>
	);
}

export {
	LayoutComponent,
}