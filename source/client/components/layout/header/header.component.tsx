import React from "react";
import { Link } from "react-router-dom";
import { APP_ROUTES} from "../../../../common/constant/appRoutes.constant";
import { useTranslation } from "react-i18next";

const HeaderComponent = () => {
	const { t, i18n } = useTranslation();

	return (
		<div>
			<div>
				<Link to={APP_ROUTES.BLUEBERRY}>{t('menu:blueberry')}</Link>
				<Link to={APP_ROUTES.RASPBERRY}>{t('menu:raspberry')}</Link>
				<Link to={APP_ROUTES.GALLERY}>{t('menu:gallery')}</Link>
				<Link to={APP_ROUTES.ABOUT}>{t('menu:about')}</Link>
			</div>
			<div>
				<span onClick={() => i18n.changeLanguage('en')}>EN</span>
				<span onClick={() => i18n.changeLanguage('hy')}>HY</span>
				<span onClick={() => i18n.changeLanguage('ru')}>RU</span>
				<span onClick={() => i18n.changeLanguage('fr')}>FR</span>
			</div>
		</div>

	)
};

export {
	HeaderComponent,
};