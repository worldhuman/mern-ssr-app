import {useHistory} from "react-router";
import {useEffect, useState} from "react";
import {getRouteConfig} from "../../routes/config";

const RouteTracker = ({children}: any) => {
  const history = useHistory();
  const [currentRoute, setRoute] = useState(history.location.pathname);

  useEffect(() => {
    const unListen = history.listen((location) => {

      if (currentRoute === location.pathname) {
        return;
      }

      const currentRouteConfig = getRouteConfig(currentRoute, ['destroy']);
      const newConfig = getRouteConfig(location.pathname, ['init']);

      if (currentRouteConfig?.destroy) {
        currentRouteConfig.destroy();
      }

      if (newConfig?.init) {
        newConfig.init();
      }

      setRoute(location.pathname);
    });
    return () => unListen();
  },[currentRoute])

  return children;
}

export {
  RouteTracker
};