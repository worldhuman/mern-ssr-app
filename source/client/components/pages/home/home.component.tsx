import React from 'react';
import { useTranslation } from 'react-i18next';

const HomeComponent = (props: any) => {
	const { t } = useTranslation();
	return <h1>Home</h1>;
};

export {
	HomeComponent,
};