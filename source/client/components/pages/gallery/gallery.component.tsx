import React from 'react';
import { GalleryBloc } from "../../../../common/bloc";
import { useValue } from 'reactive-blocs';

const GalleryComponent = () => {
	const images: any[] = useValue(GalleryBloc.data.images);

	return images?.map((im) => (
		<div>
			{im.uri}
		</div>
	));
};

export {
	GalleryComponent,
};