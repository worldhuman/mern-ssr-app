import React from 'react';
import ReactDOM from 'react-dom';
import { I18nextProvider } from "react-i18next";
import i18n from "./utils/i18n";
import './index.scss';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { APP_ROUTES } from "../common/constant/appRoutes.constant";
import { getRoutes, isSystemPage } from "../common/helper/router.helper";
import { RouteTracker } from "./components/route/routeTracker.component";
import { STORE_KEYS_BLOC_MAP } from "../common/constant/store.constant";

try {

	// @ts-ignore
	if (window.APP_STATE) {
		// @ts-ignore
		for (const [key, data] of Object.entries(window.APP_STATE)) {
			// @ts-ignore
			STORE_KEYS_BLOC_MAP[key].setData(data);
		}
	}
} catch (e) {
	console.error(e)
}

const jsx = (
	<Router>
		<I18nextProvider i18n={i18n}>
			<Route path={ APP_ROUTES.ROOT }>
				<RouteTracker>
					{ getRoutes(isSystemPage(location.pathname)) }
				</RouteTracker>
			</Route>
		</I18nextProvider>
	</Router>
);

const app = document.getElementById( "app" );
ReactDOM.hydrate(jsx, app);
