const shouldIntercept = (error: any) => {
	return error?.response?.status === 401;
};

const handleTokenRefresh = async (): Promise<any> => {
	return '';  // Todo request a new token
};

const attachTokenToRequest = (request: any, token: any) => {
	request.headers.Authorization = `Bearer ${token}`;
};

const applyRefreshTokenInterceptor = ({axiosInstance, customOptions = {}}: {axiosInstance: any; customOptions?: any}) => {
	let isRefreshing = false;
	const failedQueue: any = [];

	const options = {
		attachTokenToRequest,
		handleTokenRefresh,
		shouldIntercept,
		...customOptions,
	};

	const processQueue = (error: any, token = null) => {
		failedQueue.forEach((promiseItem: any) => {
			if (error) {
				promiseItem.reject(error);
			} else {
				promiseItem.resolve(token);
			}
		});

		failedQueue.length = 0;
	};

	const interceptor = (error: any) => {
		const originalRequest = error.config;

		if ((!!options.shouldIntercept && !options.shouldIntercept(error)) || originalRequest._retry || originalRequest._queued) {
			const status = error?.response?.status;
			if (status === 401 && originalRequest._retry) {
				console.error(error?.response);
			}
			return Promise.reject(error);
		}

		if (isRefreshing) {
			return new Promise((resolve, reject) => failedQueue.push({resolve, reject}))
				.then((token: any) => {
					originalRequest._queued = true;

					if (!!options.attachTokenToRequest) {
						options.attachTokenToRequest(originalRequest, token);
					}

					return axiosInstance.request(originalRequest);
				})
				.catch(Promise.reject)
		}
		originalRequest._retry = true;
		isRefreshing = true;
		return new Promise((resolve, reject) => {
			handleTokenRefresh()
				.then((tokenData) => {
					options.attachTokenToRequest(originalRequest, tokenData);
					processQueue(null, tokenData);
					resolve(axiosInstance.request(originalRequest));
				})
				.catch((err) => {
					processQueue(err, null);
					reject(err);
				})
				.finally(() => {
					isRefreshing = false;
				})
		});
	};

	axiosInstance.interceptors.response.use(undefined, interceptor);
};

export {
	applyRefreshTokenInterceptor,
};
