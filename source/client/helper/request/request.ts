import axios from 'axios';
import {CONSTANT_MAIN} from '../../../common/constant/main.constant';
import {EApiErrors, ERequestMethod, EResponseStatus} from "../../../common/enum/request.enum";

// applyRefreshTokenInterceptor({ axiosInstance: axios });

export interface IRequestParams {
  method?: ERequestMethod;
  params?: {[key: string]: any};
  body?: {[key: string]: any};
  headers?: {[key: string]: string};
  withCredentials?: boolean;
  responseType?: "json" | "arraybuffer" | "blob" | "document" | "text" | "stream" | undefined;
}

async function request(url: string, params: IRequestParams = {}): Promise<any> {
  const method = params.method || ERequestMethod.get;
  const data = params.body;
  const queryParams: any = { ...params.params } || {};
  const headers = { ...(params.headers || {}), 'Content-Type': 'application/json' } as any;

  // headers.Authorization = `Bearer `; // Todo set token

  try {
    const response: any = await axios({
      url,
      method,
      params: queryParams,
      responseType: params.responseType ? params.responseType : 'json',
      headers,
      data,
      baseURL: CONSTANT_MAIN.API_URL,
      withCredentials: params.withCredentials
    });

    if (response.data?.result === EResponseStatus.error) {
      const errors = response.data?.errors;

      if (errors?.length) {
        errors.forEach((error: any) => {
          console.error(error);
          if (error.code in EApiErrors) {
            // Todo show toaster or smth else
          }
        })
      }
    }

    return response.data;
  } catch (error) {
    console.error(error);
  }
}

export {
  request
}