import {APP_ROUTES, SYSTEM_ROUTES} from "../../common/constant/appRoutes.constant";
import {HomeComponent} from "../components/pages/home/home.component";
import {BlueberryComponent} from "../components/pages/blueberry/blueberry.component";
import {RaspberryComponent} from "../components/pages/raspberry/raspberry.component";
import {GalleryComponent} from "../components/pages/gallery/gallery.component";
import {AboutComponent} from "../components/pages/about/about.component";
import {ContactComponent} from "../components/pages/contact/contact.component";
import {LoginComponent} from "../components/admin/login/login.component";
import {SystemComponent} from "../components/admin/system/system.component";
import {NoMatchComponent} from "../components/pages/noMatch/noMatch.component";
import pick from "lodash/pick";
import {GalleryBloc} from "../../common/bloc";

const routesConfig = {
  [APP_ROUTES.ROOT]: {
    path: APP_ROUTES.ROOT,
    component: HomeComponent,
    exact: true,
  },
  [APP_ROUTES.BLUEBERRY]: {
    path: APP_ROUTES.BLUEBERRY,
    component: BlueberryComponent,
    exact: true,
    init: async () => console.log('Init'),
    destroy: () => console.log('destroy')
  },
  [APP_ROUTES.RASPBERRY]: {
    path: APP_ROUTES.RASPBERRY,
    component: RaspberryComponent,
    exact: true,
  },
  [APP_ROUTES.GALLERY]: {
    path: APP_ROUTES.GALLERY,
    component: GalleryComponent,
    init: () => GalleryBloc.start(),
    exact: true,
  },
  [APP_ROUTES.ABOUT]: {
    path: APP_ROUTES.ABOUT,
    component: AboutComponent,
    exact: true,
  },
  [APP_ROUTES.CONTACT]: {
    path: APP_ROUTES.CONTACT,
    component: ContactComponent,
    exact: true,
  },
  [SYSTEM_ROUTES.LOGIN]: {
    path: SYSTEM_ROUTES.LOGIN,
    component: LoginComponent,
    exact: true,
  },
  [APP_ROUTES.NOMATCH]: {
    path: '*',
    component: NoMatchComponent,
    exact: false,
  },
  [SYSTEM_ROUTES.SYSTEM]: {
    path: SYSTEM_ROUTES.SYSTEM,
    component: SystemComponent,
    exact: true,
  },
};

const getRouteConfig = (route: string, fields = ['exact', 'path', 'component']): any =>
  pick(routesConfig[route], ...fields);

export {
  routesConfig,
  getRouteConfig
};