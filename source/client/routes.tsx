import React from "react";
import {Route, Switch} from "react-router-dom";
import {AppComponent} from "./components/app/app.component";
import {SystemAppComponent} from "./components/admin/systemApp.component";
import {APP_ROUTES, SYSTEM_ROUTES} from "../common/constant/appRoutes.constant";
import {getRouteConfig} from "./routes/config";

const adminRoutes = (
	<SystemAppComponent>
		<Switch>
			{
				Object
					.values(SYSTEM_ROUTES)
					.map((route) => <Route key={route} {...getRouteConfig(route)} />)
			}
		</Switch>
	</SystemAppComponent>
);

const commonRoutes = (
	<AppComponent>
		<Switch>
			{
				Object
					.values(APP_ROUTES)
					.map((route) => <Route key={route} {...getRouteConfig(route)} />)
			}
		</Switch>
	</AppComponent>
);

export {
	adminRoutes,
	commonRoutes,
}