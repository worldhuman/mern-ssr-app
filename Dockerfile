FROM node:14-slim

WORKDIR /application

COPY . .

RUN ['npm install', 'npm prod:build', 'npm prod:start']